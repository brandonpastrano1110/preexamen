package modelo;

/**
 *
 * @author Brandon
 */
public class CuentaBan {
    String numCuenta, fechaAp, nomBanco;
    Cliente datosCli;
    float porRend, saldo;
    public CuentaBan(){
        this.numCuenta="";
        this.fechaAp="";
        this.nomBanco="";
        this.datosCli=null;
        this.porRend=0.0f;
        this.saldo=0.0f;
    }
    public CuentaBan(CuentaBan cu){
        this.numCuenta=cu.numCuenta;
        this.fechaAp=cu.fechaAp;
        this.nomBanco=cu.nomBanco;
        this.datosCli=cu.datosCli;
        this.porRend=cu.porRend;
        this.saldo=cu.saldo;
    }
    public CuentaBan(String numCuenta, String fechaAp, String nomBanco, Cliente datosCli, float porRend, float saldo){
        this.numCuenta=numCuenta;
        this.fechaAp=fechaAp;
        this.nomBanco=nomBanco;
        this.datosCli=datosCli;
        this.porRend=porRend;
        this.saldo=saldo;
    }
    public void setDatosCli(Cliente cli){
        this.datosCli=cli;
    }
    public void setNumCuenta(String numC){
        this.numCuenta=numC;
    }
    public void setFechaAp(String dat){
        this.fechaAp=dat;
    }
    public void setNomBanco(String nomB){
        this.nomBanco=nomB;
    }
    public void setPorSend(float porS){
        this.porRend=porS;
    }
    public void setSaldo(float saldo){
        this.saldo=saldo;
    } 
    public String getNumCuenta(){
        return this.numCuenta;
    }
    public String getFechaAp(){
        return this.fechaAp;
    }
    public String getNomBanco(){
        return this.nomBanco;
    }
    public float getPorSend(){
        return this.porRend;
    }
    public float getSaldo(){
        return this.saldo;
    } 
    public void depositar(float dep){
        saldo=saldo+dep;
    }
    public boolean retirar(float ret){
        if(ret<=this.saldo){
            this.saldo=this.saldo-ret;
            return true;
        }
        else{
            return false;
        }
    }
    public float calcularRe(){
        float calRenDi=0;
        calRenDi=this.porRend*(saldo/365);
        return calRenDi;
    }
}

