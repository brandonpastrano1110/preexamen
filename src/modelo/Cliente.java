 
package modelo;
 
public class Cliente {
    String nomCliente, fechaNa, domicilio;
    int sexo;
    
    public Cliente(){
        this.nomCliente="";
        this.fechaNa="";
        this.domicilio="";
        this.sexo=0;
    }
    public Cliente(Cliente cli){
        this.nomCliente=cli.nomCliente;
        this.fechaNa=cli.fechaNa;
        this.domicilio=cli.domicilio;
        this.sexo=cli.sexo;
    }
    public Cliente(String nomCliente, String fechaNa, String domicilio, int sexo){
        this.nomCliente=nomCliente;
        this.fechaNa=fechaNa;
        this.domicilio=domicilio;
        this.sexo=sexo;
    }
    public void setNomCliente(String nom){
        this.nomCliente=nom;
    }
    public void setFechaNa(String fe){
        this.fechaNa=fe;
    }
    public void setDomicilio(String dom){
        this.domicilio=dom;
    }
    public void setSexo(int sex){
        this.sexo=sex; 
    }
    public String getNomCliente(){
        return this.nomCliente;
    }
    public String getFechaNa(){
        return this.fechaNa;
    }
    public String getDomicilio(){
        return this.domicilio;
    }
    public int getSexo(){
        return this.sexo;
    }
}
