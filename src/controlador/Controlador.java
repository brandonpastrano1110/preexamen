package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Cliente;
import modelo.CuentaBan;
import vista.dlgCuentaBan;
/**
 *
 * @author Brandon
 */
public class Controlador implements ActionListener{
    private dlgCuentaBan vista;
    private Cliente cli;
    private CuentaBan cue; 
    Cliente clie = new Cliente();
    float cantidad;
    private void iniciarVista(){
        vista.setTitle("::BANCO PANA::");
        vista.setSize(800, 800);
        vista.setVisible(true);
    }
    
    public Controlador(CuentaBan cue, dlgCuentaBan vista){
        this.vista = vista;
        this.cue= cue;
        this.cli = cli;
        
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnHacerDep.addActionListener(this);
        vista.btnHacerRet.addActionListener(this);
        vista.rbtnMas.addActionListener(this);
        vista.rbtnFem.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
    }   
    public void btnLimpiar(){
        vista.txtNumCuenta.setText("");
            vista.txtFechaAp.setText("");
            vista.txtNomBanco.setText("");
            vista.txtPorRed.setText("");
            vista.txtSaldo.setText("");
            vista.txtNombre.setText("");
            vista.txtDomicilio.setText("");
            vista.txtFechaNac.setText("");
            vista.btnGrup2.clearSelection();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.btnNuevo){
            vista.txtCantidad.setEnabled(true);
            vista.txtNumCuenta.setEnabled(true);
            vista.txtFechaNac.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtNomBanco.setEnabled(true);
            vista.txtFechaAp.setEnabled(true);
            vista.txtPorRed.setEnabled(true);
            vista.txtSaldo.setEnabled(true); 
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.rbtnFem.setEnabled(true);
            vista.rbtnMas.setEnabled(true);
        }
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 vista.dispose();
                 System.exit(0);
             }
        }
        if(e.getSource()==vista.btnGuardar){
             try{
                 cue.setNumCuenta(vista.txtNumCuenta.getText());
                 cue.setFechaAp(vista.txtFechaAp.getText());
                 cue.setNomBanco(vista.txtNomBanco.getText());
                 clie.setNomCliente(vista.txtNombre.getText());
                 clie.setFechaNa(vista.txtFechaNac.getText());
                 clie.setDomicilio(vista.txtDomicilio.getText());
                 cue.setPorSend(Float.parseFloat(vista.txtPorRed.getText()));
                 cue.setSaldo(Float.parseFloat(vista.txtSaldo.getText()));
                 if(vista.rbtnFem.isSelected()){
                    clie.setSexo(1); 
                 }
                 else if(vista.rbtnMas.isSelected()){
                    clie.setSexo(2);
                 }
                 vista.btnHacerDep.setEnabled(true);
                 vista.btnHacerRet.setEnabled(true);
                 vista.txtSaldo.setEnabled(false);
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
        }
        if(e.getSource()==vista.btnLimpiar){
            btnLimpiar();
        }
        if(e.getSource()==vista.btnMostrar){
            vista.txtNumCuenta.setText(cue.getNumCuenta());
            vista.txtFechaAp.setText(cue.getFechaAp());
            vista.txtNomBanco.setText(cue.getNomBanco());
            vista.txtPorRed.setText(Float.toString(cue.getPorSend()));
            vista.txtSaldo.setText(Float.toString(cue.getSaldo()));
            vista.txtNombre.setText(clie.getNomCliente());
            vista.txtDomicilio.setText(clie.getDomicilio());
            vista.txtFechaNac.setText(clie.getFechaNa()); 
            if(clie.getSexo()==1){ 
                 vista.rbtnFem.setSelected(true);
                
            }
            else{ 
                vista.rbtnMas.setSelected(true); 
            }
            vista.txtCantidad.setText(Float.toString(cantidad));
        }
        if(e.getSource()==vista.btnHacerDep){
            try{
                cantidad=Float.parseFloat(vista.txtCantidad.getText());
                cue.depositar(cantidad);
                JOptionPane.showMessageDialog(vista,"Se deposito con exito");
                vista.txtNuevoSaldo.setText(Float.toString(cue.getSaldo()));
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
        }
        if(e.getSource()==vista.btnHacerRet){
            try{
                cantidad=Float.parseFloat(vista.txtCantidad.getText());
                if(cue.retirar(cantidad)==true){
                    JOptionPane.showMessageDialog(vista,"Se retiro con exito");
                    vista.txtNuevoSaldo.setText(Float.toString(cue.getSaldo()));
                }
                else{
                    int option=JOptionPane.showConfirmDialog(vista,"No tiene fondo suficiente ¿Deseas volver a intentar?",
                            "Decide", JOptionPane.YES_NO_OPTION);
                    if(option==JOptionPane.YES_NO_OPTION){
                        vista.txtCantidad.setText("");
                        vista.txtCantidad.requestFocus(); 
                    }
                }
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CuentaBan cue = new CuentaBan();
        dlgCuentaBan vista = new dlgCuentaBan(new JFrame(), true);
        Controlador contra = new Controlador(cue, vista);
        contra.iniciarVista();
    }
}
